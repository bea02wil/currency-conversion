import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Header from "./components/layout/Header";
import Rate from "./components/pages/Rate";
import Home from "./components/pages/Home";
import store from "./store";
import { Provider } from "react-redux";

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <Header />
        <Switch>
          <Route exact path="/" component={Home}></Route>
          <Route exact path="/rates" component={Rate}></Route>
        </Switch>
      </Router>
    </Provider>
  );
};

export default App;
