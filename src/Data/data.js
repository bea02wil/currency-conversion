export const culcboxesData = [
  {
    dateTime: false,
  },
  {
    dateTime: true,
  },
];

//Т.к в API нет имен валют, а есть только коды со значениями, создаю такой массив
export const currenciesData = [
  { name: "Российский рубль", code: "RUB" },
  { name: "Доллар США", code: "USD" },
  { name: "Евро", code: "EUR" },
  { name: "Фунт стерлингов", code: "GBP" },
  { name: "Австралийский доллар", code: "AUD" },
  { name: "Болгарский лев", code: "BGN" },
  { name: "Бразильский реал", code: "BRL" },
  { name: "Венгерский форинт", code: "HUF" },
  { name: "Вон Республики Корея", code: "KRW" },
  { name: "Датская крона", code: "DKK" },
  { name: "Индийская рупия", code: "INR" },
  { name: "Канадский доллар", code: "CAD" },
  { name: "Китайский юань", code: "CNY" },
  { name: "Новый румынский лей", code: "RON" },
  { name: "Норвежская крона", code: "NOK" },
  { name: "Польский злотый", code: "PLN" },
  { name: "Сингапурский доллар", code: "SGD" },
  { name: "Турецкая лира", code: "TRY" },
  { name: "Чешская крона", code: "CZK" },
  { name: "Шведская крона", code: "SEK" },
  { name: "Швейцарский франк", code: "CHF" },
  { name: "Южноафриканский рэнд", code: "ZAR" },
  { name: "Японская иена", code: "JPY" },
  { name: "Исландская крона", code: "ISK" },
  { name: "Хорватская куна", code: "HRK" },
  { name: "Гонконгский доллар", code: "HKD" },
  { name: "Индонезийская рупия", code: "IDR" },
  { name: "Новый израильский шекель", code: "ILS" },
  { name: "Мексиканское песо", code: "MXN" },
  { name: "Малайзийский ринггит", code: "MYR" },
  { name: "Новозеландский доллар", code: "NZD" },
  { name: "Филиппинское песо", code: "PHP" },
  { name: "Тайский бат", code: "THB" },
  { name: "Аргентинское песо", code: "ARS" },
  { name: "Алжирский динар", code: "DZD" },
  { name: "Марокканский дирхам", code: "MAD" },
  { name: "Новый тайваньский доллар", code: "TWD" },
  { name: "Биткойн", code: "BTC" },
  { name: "Эфириум", code: "ETH" },
  { name: "Бинанс коин", code: "BNB" },
  { name: "Дожкоин", code: "DOGE" },
  { name: "Рипл", code: "XRP" },
  { name: "Биткойн Кэш", code: "BCH" },
  { name: "Лайткоин", code: "LTC" },
];
