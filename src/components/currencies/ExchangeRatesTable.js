import { useState, useEffect } from "react";
import { currenciesData } from "../../Data/data";
import PropTypes from "prop-types";
import classes from "../../App.module.scss";
import axios from "axios";

const ExchangeRatesTable = ({ selectValue }) => {
  const [exchangeRates, setExchangeRates] = useState([]);

  const getCurrency = (callback) => {
    return currenciesData.find(callback);
  };

  const sortByName = (a, b) => {
    if (a[2] > b[2]) {
      return 1;
    }
    if (a[2] < b[2]) {
      return -1;
    }
    return 0;
  };

  const addNamesToRates = (exchangeRates) => {
    const exchangeRatesArr = Object.entries(exchangeRates);

    const ratesWithNames = exchangeRatesArr.map((exchangeRate) => [
      exchangeRate[0],
      exchangeRate[1],
      getCurrency((currency) => currency.code === exchangeRate[0]).name,
    ]);

    return ratesWithNames;
  };

  const getExchangeRates = async () => {
    try {
      const currency = getCurrency((currency) => currency.name === selectValue);
      const base = currency.code;

      const res = await axios.get(
        `${process.env.REACT_APP_API_URL}/live/?api_key=${process.env.REACT_APP_ABSTRACT_API_KEY}&base=${base}`
      );

      const exchangeRatesWithNames = addNamesToRates(res.data.exchange_rates);

      const sortedExchangeRates = exchangeRatesWithNames.sort(sortByName);

      setExchangeRates(sortedExchangeRates);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getExchangeRates();

    // eslint-disable-next-line
  }, [selectValue]);

  return (
    <>
      <div className={classes.scrollTable}>
        <table>
          <thead>
            <tr>
              <th>Rate</th>
              <th>Value</th>
            </tr>
          </thead>
        </table>
        <div className={classes.scrollTableBody}>
          <table>
            <tbody>
              {exchangeRates.map((rateArr) => (
                <tr key={rateArr[0]}>
                  <td>
                    {rateArr[2]} <span>{rateArr[0]}</span>
                  </td>
                  <td>
                    {rateArr[1] > 0.0001 ? rateArr[1].toFixed(4) : rateArr[1]}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
      <h3 className={classes.totalRates}>
        Всего обменных курсов: <span>{exchangeRates.length}</span>
      </h3>
    </>
  );
};

ExchangeRatesTable.propTypes = {
  selectValue: PropTypes.string.isRequired,
};

export default ExchangeRatesTable;
