import classes from "../../App.module.scss";
import PropTypes from "prop-types";
import { useSelector, useDispatch } from "react-redux";
import {
  selectCurrencyItem,
  setConvertedValue,
} from "../../actions/culcboxActions";

const CurrencyListItem = ({ currencyName, currencyCode }) => {
  const culcboxState = useSelector((state) => state.culcbox);
  const dispatchToCulcboxReducer = useDispatch(culcboxState);

  const selectItem = () => {
    const currency = { name: currencyName, code: currencyCode };

    const uniqueIdConvertByFirstCulcbox = 0;
    let uniqueIdSelectedCode = null;

    const isSelectedFromFirstCulcbox =
      culcboxState.selectedCurrencyCodes.ofFirstCulcbox.length === 2;

    isSelectedFromFirstCulcbox
      ? (uniqueIdSelectedCode = 0)
      : (uniqueIdSelectedCode = 1);

    dispatchToCulcboxReducer(
      selectCurrencyItem({ currency, uniqueId: uniqueIdSelectedCode })
    );

    dispatchToCulcboxReducer(setConvertedValue(uniqueIdConvertByFirstCulcbox));
  };

  return (
    <div className={classes.currencyRow} onClick={selectItem}>
      <div className={classes.currencyNameOfList}>{currencyName}</div>
      <div className={classes.currencyCode}>{currencyCode}</div>
    </div>
  );
};

CurrencyListItem.propTypes = {
  currencyName: PropTypes.string.isRequired,
  currencyCode: PropTypes.string.isRequired,
};

export default CurrencyListItem;
