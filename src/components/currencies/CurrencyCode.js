import PropTypes from "prop-types";
import classes from "../../App.module.scss";

const CurrencyCode = ({ active, name, code, onClick }) => {
  const currencyClass = classes.currency;
  const activeCurrencyClass = classes.activeCurrency;

  const bothClasses = [currencyClass, activeCurrencyClass].join(" ");
  return (
    <div
      currency-name={name}
      className={active ? bothClasses : currencyClass}
      onClick={onClick}
    >
      {code}
    </div>
  );
};

CurrencyCode.propTypes = {
  active: PropTypes.bool.isRequired,
  name: PropTypes.string,
  code: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  onClick: PropTypes.func.isRequired,
};

export default CurrencyCode;
