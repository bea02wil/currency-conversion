import classes from "../../App.module.scss";
import CurrencyItem from "../currencies/CurrencyListItem";
import { currenciesData } from "../../Data/data";

const CurrencyList = () => {
  const prepareColumnsForMapByCount = (count) => {
    const columns = [];
    for (let i = 1; i <= count; i++) {
      columns.push(i);
    }
    return columns;
  };

  const sortByName = (a, b) => {
    if (a.name > b.name) {
      return 1;
    }
    if (a.name < b.name) {
      return -1;
    }
    return 0;
  };

  //Сортировка выбранных элементов в массиве с индекса по индекс.
  const partSort = (arr, indexFrom, indexTo, sortFunc) => {
    const exactIndexFrom = Math.min(indexFrom, indexTo);
    const exactIndexTo = Math.max(indexFrom, indexTo);

    const tempArr = new Array(exactIndexTo - exactIndexFrom + 1);
    tempArr.fill(0);
    let j = 0;
    for (let i = exactIndexFrom; i <= exactIndexTo; i++) {
      tempArr[j] = arr[i];
      j++;
    }

    tempArr.sort(sortFunc);

    j = 0;
    for (let i = exactIndexFrom; i <= exactIndexTo; i++) {
      arr[i] = tempArr[j];
      j++;
    }
  };

  //Все расчеты ниже для того, чтобы автоматически выстраивал кол-во элементов в колонках
  let columnsCount = Math.trunc(currenciesData.length / 10);

  if (columnsCount === 0) {
    columnsCount = 1;
  }

  const currenciesCopy = [...currenciesData];
  partSort(currenciesCopy, 4, currenciesCopy.length - 1, sortByName);

  const getLastIndexInChunk = () => {
    const restColumnsCount = Math.trunc(currenciesCopy.length / 10);
    const lastIndex = Math.ceil(currenciesCopy.length / restColumnsCount);

    return lastIndex;
  };

  const columns = prepareColumnsForMapByCount(columnsCount);

  return (
    <div className={classes.currencyList}>
      {columns.map((column, index) => {
        const lastIndexInChunk = getLastIndexInChunk();
        const currenciesChunk = currenciesCopy.slice(0, lastIndexInChunk);
        currenciesCopy.splice(0, lastIndexInChunk);

        return (
          <div className={classes.currencyColumn} key={index}>
            {currenciesChunk.map((currency, index) => (
              <CurrencyItem
                key={index}
                currencyName={currency.name}
                currencyCode={currency.code}
              />
            ))}
          </div>
        );
      })}
    </div>
  );
};

export default CurrencyList;
