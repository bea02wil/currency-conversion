import { NavLink } from "react-router-dom";
import classes from "../../App.module.scss";

const Header = () => {
  const links = [
    {
      to: "/",
      name: "Home",
    },
    {
      to: "/rates",
      name: "Current exchange rates",
    },
  ];

  return (
    <header className={classes.header}>
      <h1 className={classes.headerTitle}>Currency Conversion</h1>
      <nav className={classes.links}>
        <ul>
          {links.map((link, index) => (
            <li key={index}>
              <NavLink to={link.to} exact activeClassName={classes.active}>
                {link.name}
              </NavLink>
            </li>
          ))}
        </ul>
      </nav>
    </header>
  );
};

export default Header;
