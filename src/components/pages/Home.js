import classes from "../../App.module.scss";
import reverseArrow from "../../img/transfer.svg";
import Culcboxes from "../culcboxes/Culcboxes";
import CurrencyList from "../currencies/CurrencyList";
import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  getMainCurrencies,
  setConvertedValue,
  swapSelectedCurrencies,
  setCaretIconSelectedStatus,
  switchCaretIcon,
  closeCurrencyList,
} from "../../actions/culcboxActions";

const Home = () => {
  const culcboxState = useSelector((state) => state.culcbox);
  const dispatchToCulcboxReducer = useDispatch(culcboxState);

  const uniqueId = 0;

  useEffect(() => {
    dispatchToCulcboxReducer(getMainCurrencies());

    dispatchToCulcboxReducer(setConvertedValue(uniqueId, 1000));
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    const closeCurrencyListHandler = (e) => {
      const targetClassName = e.target.className;
      const caretUpClassName = "fas fa-caret-up";

      if (culcboxState.caretIconSelectedStatus === true) {
        if (
          !targetClassName.includes(classes.currency) &&
          targetClassName !== caretUpClassName
        ) {
          const status = false;

          dispatchToCulcboxReducer(closeCurrencyList());
          dispatchToCulcboxReducer(switchCaretIcon());
          dispatchToCulcboxReducer(setCaretIconSelectedStatus({ status }));
        }
      }
    };

    document.body.addEventListener("click", closeCurrencyListHandler);

    return () => {
      document.body.removeEventListener("click", closeCurrencyListHandler);
    };
    // eslint-disable-next-line
  }, [culcboxState.caretIconSelectedStatus]);

  const swapSelectedCurrenciesHandler = () => {
    const firstSelectedCurrencyCode =
      culcboxState.selectedCurrencyCodes.ofFirstCulcbox[0];

    const secondSelectedCurrencyCode =
      culcboxState.selectedCurrencyCodes.ofSecondCulcbox[0];

    const firstCurrencyToSwap = culcboxState.currencies.ofFirstCulcbox.find(
      (currency) => currency.code === firstSelectedCurrencyCode
    );

    const secondCurrencyToSwap = culcboxState.currencies.ofSecondCulcbox.find(
      (currency) => currency.code === secondSelectedCurrencyCode
    );

    const swappedCurrencies = {
      ofFirstCulcbox: firstCurrencyToSwap,
      ofSecondCulcbox: secondCurrencyToSwap,
    };

    if (firstCurrencyToSwap.code !== secondCurrencyToSwap.code) {
      dispatchToCulcboxReducer(swapSelectedCurrencies(swappedCurrencies));

      dispatchToCulcboxReducer(setConvertedValue(uniqueId));
    }
  };

  return (
    <main className={classes.converting}>
      <img
        className={classes.reverse}
        src={reverseArrow}
        alt=""
        onClick={swapSelectedCurrenciesHandler}
      />

      {culcboxState.isCurrencyListOpen && <CurrencyList />}
      <Culcboxes />
    </main>
  );
};

export default Home;
