import { useState } from "react";
import classes from "../../App.module.scss";
import SelectCurrency from "../select/SelectCurrency";
import ExchangeRatesTable from "../currencies/ExchangeRatesTable";

const Rate = () => {
  const [selectedRate, setSelectedRate] = useState("Российский рубль");

  const getSelectValue = (e) => {
    const selectValue = e.target.value;

    setSelectedRate(selectValue);
  };

  return (
    <div className={classes.currentExchangeRate}>
      <SelectCurrency selectValue={selectedRate} onChange={getSelectValue} />

      <ExchangeRatesTable selectValue={selectedRate} />
    </div>
  );
};

export default Rate;
