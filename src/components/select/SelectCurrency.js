import classes from "../../App.module.scss";
import PropTypes from "prop-types";
import { currenciesData } from "../../Data/data";

const SelectCurrency = ({ selectValue, onChange }) => {
  return (
    <select
      className={classes.selectBaseRate}
      value={selectValue}
      onChange={onChange}
    >
      {currenciesData.map((currency) => (
        <option key={currency.code}>{currency.name}</option>
      ))}
    </select>
  );
};

SelectCurrency.propTypes = {
  onChange: PropTypes.func.isRequired,
};

export default SelectCurrency;
