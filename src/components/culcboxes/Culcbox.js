import classes from "../../App.module.scss";
import { useEffect } from "react";
import PropTypes from "prop-types";
import useDebounce from "../../customHooks/useDebounce";
import { useSelector, useDispatch } from "react-redux";
import CurrencyCode from "../currencies/CurrencyCode";
import {
  setCurrencyCodeSelected,
  switchCaretIcon,
  setCaretIconSelectedStatus,
  showCurrencyList,
  closeCurrencyList,
  setCurrencyValue,
  setConvertedValue,
} from "../../actions/culcboxActions";

const Culcbox = ({ uniqueId, type, dateTime }) => {
  const culcboxState = useSelector((state) => state.culcbox);
  const dispatchToCulcboxReducer = useDispatch(culcboxState);

  const currencies =
    uniqueId === 0
      ? culcboxState.currencies?.ofFirstCulcbox
      : culcboxState.currencies?.ofSecondCulcbox;

  const currencyValue =
    uniqueId === 0
      ? culcboxState.currencyValue.ofFirstCulcbox
      : culcboxState.currencyValue.ofSecondCulcbox;

  const debouncedCurrencyValue = useDebounce(currencyValue, 300);

  const baseCurrency =
    uniqueId === 0
      ? culcboxState.baseCurrency.ofFirstCulcbox
      : culcboxState.baseCurrency.ofSecondCulcbox;

  const selectedCurrencyCode =
    uniqueId === 0
      ? culcboxState.selectedCurrencyCodes.ofFirstCulcbox[0]
      : culcboxState.selectedCurrencyCodes.ofSecondCulcbox[0];

  const selectedCurrencyCodeAnotherCulcbox =
    uniqueId === 0
      ? culcboxState.selectedCurrencyCodes.ofSecondCulcbox[0]
      : culcboxState.selectedCurrencyCodes.ofFirstCulcbox[0];

  const selectedCurrencyCodes =
    uniqueId === 0
      ? culcboxState.selectedCurrencyCodes.ofFirstCulcbox
      : culcboxState.selectedCurrencyCodes.ofSecondCulcbox;

  const setCurrencyValueHandler = (e) => {
    const numbers = /^$|^\.?[0-9]*$|^[0-9]+\.?[0-9]*$/;
    let value = e.target.value;

    if (value.match(numbers)) {
      dispatchToCulcboxReducer(setCurrencyValue(value, uniqueId));
    }
  };

  const onClickCurrencyCodeHandler = (currency, index) => {
    let isCaretDown = false;

    if (index === currencies.length - 1) {
      isCaretDown = true;

      dispatchToCulcboxReducer(setCurrencyCodeSelected({ uniqueId, index }));

      dispatchToCulcboxReducer(
        setCaretIconSelectedStatus({ status: true, uniqueId })
      );

      if (!culcboxState.isCurrencyListOpen) {
        dispatchToCulcboxReducer(showCurrencyList());
      }
    } else {
      dispatchToCulcboxReducer(
        setCurrencyCodeSelected({ uniqueId, code: currency.code })
      );

      if (currencyValue) {
        const uniqueIdConvertByFirstCulcbox = 0;

        dispatchToCulcboxReducer(
          setConvertedValue(uniqueIdConvertByFirstCulcbox)
        );
      }

      if (culcboxState.isCurrencyListOpen) {
        dispatchToCulcboxReducer(closeCurrencyList());
      }
    }
    dispatchToCulcboxReducer(switchCaretIcon({ isCaretDown, uniqueId }));
  };

  useEffect(() => {
    //debauncing
    const activeElement = document.activeElement;

    const convertValue = () => {
      if (
        uniqueId === +activeElement.id &&
        activeElement.tagName === "INPUT" &&
        currencyValue
      ) {
        dispatchToCulcboxReducer(setConvertedValue(uniqueId, currencyValue));
      }
    };

    if (debouncedCurrencyValue) {
      convertValue();
    }

    // eslint-disable-next-line
  }, [debouncedCurrencyValue]);

  return (
    <>
      <div className={classes.culcBox}>
        <div className={classes.currencyWrapper}>
          {currencies.map((currency, index) => (
            <CurrencyCode
              key={index}
              name={currency.name}
              code={currency.code || currency}
              active={
                selectedCurrencyCodes.includes(currency.code) ||
                (selectedCurrencyCodes.includes(index) &&
                  culcboxState.caretIconSelectedStatus === true)
              }
              onClick={() => onClickCurrencyCodeHandler(currency, index)}
            />
          ))}
        </div>

        <div className={classes.inputBox}>
          <input
            id={uniqueId}
            className={classes.inputBoxInput}
            type={type}
            autoComplete="off"
            value={currencyValue}
            maxLength="10"
            onChange={(e) => setCurrencyValueHandler(e)}
          />
          {!currencyValue ? null : (
            <div className={classes.inputBoxRate}>
              1 {selectedCurrencyCode} ={" "}
              {(baseCurrency && baseCurrency.toFixed(4)) || 1}{" "}
              {selectedCurrencyCodeAnotherCulcbox}
            </div>
          )}
        </div>
        {dateTime && currencyValue ? (
          <div className={classes.culcBoxDateTime}>
            Данные за {culcboxState.dateTime}
          </div>
        ) : null}
      </div>
    </>
  );
};

Culcbox.defaultProps = {
  type: "text",
};

Culcbox.propTypes = {
  uniqueId: PropTypes.number.isRequired,
  type: PropTypes.string.isRequired,
  dateTime: PropTypes.bool.isRequired,
};

export default Culcbox;
