import Culcbox from "./Culcbox";
import { culcboxesData } from "../../Data/data";

const Culcboxes = () => {
  return culcboxesData.map((culcbox, index) => (
    <Culcbox
      key={index}
      uniqueId={index}
      // type={culcbox.type}
      // placeholder={culcbox.placeholder}
      // size={culcbox.size}
      // defaultCurrencySelectedSign={culcbox.defaultCurrencySelectedSign}
      {...culcbox}
    />
  ));
};

export default Culcboxes;
