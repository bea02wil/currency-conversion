import {
  SET_CURRENCY_CODE_SELECTED,
  SWAP_SELECTED_CURRENCIES,
  GET_MAIN_CURRENCIES,
  SWITCH_CARET_ICON,
  SET_CARET_ICON_SELECTED_STATUS,
  SHOW_CURRENCY_LIST,
  CLOSE_CURRENCY_LIST,
  SELECT_CURRENCY_ITEM,
  SET_CURRENCY_VALUE,
  SET_CONVERTED_VALUE,
} from "./types";
import axios from "axios";

let cachedApiData = [];

export const setCurrencyCodeSelected = (selectedCurrency) => {
  return {
    type: SET_CURRENCY_CODE_SELECTED,
    payload: selectedCurrency,
  };
};

export const swapSelectedCurrencies = (swappedCurrencies) => {
  return {
    type: SWAP_SELECTED_CURRENCIES,
    payload: swappedCurrencies,
  };
};

export const getMainCurrencies = () => {
  return {
    type: GET_MAIN_CURRENCIES,
  };
};

export const switchCaretIcon = (uniqueId) => {
  return {
    type: SWITCH_CARET_ICON,
    payload: uniqueId,
  };
};

export const setCaretIconSelectedStatus = (status) => {
  return {
    type: SET_CARET_ICON_SELECTED_STATUS,
    payload: status,
  };
};

export const showCurrencyList = () => {
  return {
    type: SHOW_CURRENCY_LIST,
  };
};

export const closeCurrencyList = () => {
  return {
    type: CLOSE_CURRENCY_LIST,
  };
};

export const selectCurrencyItem = (currency) => {
  return {
    type: SELECT_CURRENCY_ITEM,
    payload: currency,
  };
};

export const setCurrencyValue = (value, uniqueId) => {
  return {
    type: SET_CURRENCY_VALUE,
    payload: { value, uniqueId },
  };
};

export const setConvertedValue =
  (uniqueId, value) => async (dispatch, getState) => {
    try {
      const isInCachedApiData = (cachedApiData, ...comparedValues) => {
        return cachedApiData?.some(
          (data) =>
            data.value === comparedValues[0] &&
            data.baseCurrencyFirstCulcbox === comparedValues[1] &&
            data.baseCurrencySecondCulcbox === comparedValues[2]
        );
      };
      const isCachedApiDataEmpty = () => {
        return cachedApiData.length <= 0;
      };

      let cachedObj = null;

      let exchangeRatesFirstCulcbox = null;
      let exchangeRatesSecondCulcbox = null;

      let lastUpdated = null;

      const currentState = getState().culcbox;

      const baseCurrencyFirstCulcbox =
        currentState.selectedCurrencyCodes.ofFirstCulcbox[0];

      const baseCurrencySecondCulcbox =
        currentState.selectedCurrencyCodes.ofSecondCulcbox[0];

      if (
        !isInCachedApiData(
          cachedApiData,
          value,
          baseCurrencyFirstCulcbox,
          baseCurrencySecondCulcbox
        )
      ) {
        const resFirstCulcbox = await axios.get(
          `${process.env.REACT_APP_API_URL}/live/?api_key=${process.env.REACT_APP_ABSTRACT_API_KEY}&base=${baseCurrencyFirstCulcbox}`
        );

        const resSecondCulcbox = await axios.get(
          `${process.env.REACT_APP_API_URL}/live/?api_key=${process.env.REACT_APP_ABSTRACT_API_KEY}&base=${baseCurrencySecondCulcbox}`
        );

        exchangeRatesFirstCulcbox = resFirstCulcbox.data.exchange_rates;

        exchangeRatesSecondCulcbox = resSecondCulcbox.data.exchange_rates;

        lastUpdated =
          uniqueId === 0
            ? resFirstCulcbox.data.last_updated
            : resSecondCulcbox.data.last_updated;

        if (!isCachedApiDataEmpty()) {
          const lastCachedObj = cachedApiData[cachedApiData.length - 1];
          if (lastUpdated !== lastCachedObj.lastUpdated) {
            cachedApiData = [];
          }
        }

        cachedApiData.push({
          value,
          baseCurrencyFirstCulcbox,
          baseCurrencySecondCulcbox,
          exchangeRatesFirstCulcbox,
          exchangeRatesSecondCulcbox,
          lastUpdated,
        });
      } else {
        cachedObj = cachedApiData.find(
          (obj) =>
            obj.value === value &&
            obj.baseCurrencyFirstCulcbox === baseCurrencyFirstCulcbox &&
            obj.baseCurrencySecondCulcbox === baseCurrencySecondCulcbox
        );
      }

      dispatch({
        type: SET_CONVERTED_VALUE,
        payload: {
          value,
          exchangeRatesFirstCulcbox:
            exchangeRatesFirstCulcbox || cachedObj.exchangeRatesFirstCulcbox,
          exchangeRatesSecondCulcbox:
            exchangeRatesSecondCulcbox || cachedObj.exchangeRatesSecondCulcbox,
          lastUpdated: lastUpdated || cachedObj.lastUpdated,
          uniqueId,
        },
      });
    } catch (err) {
      console.log(err);
    }
  };
