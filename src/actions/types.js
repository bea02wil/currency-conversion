export const SET_CURRENCY_CODE_SELECTED = "SET_CURRENCY_CODE_SELECTED";
export const SWAP_SELECTED_CURRENCIES = "SWAP_SELECTED_CURRENCIES";
export const GET_MAIN_CURRENCIES = "GET_MAIN_CURRENCIES";
export const SWITCH_CARET_ICON = "SWITCH_CARET_ICON";
export const SET_CARET_ICON_SELECTED_STATUS = "SET_CARET_ICON_SELECTED_STATUS";
export const SHOW_CURRENCY_LIST = "SHOW_CURRENCY_LIST";
export const CLOSE_CURRENCY_LIST = "CLOSE_CURRENCY_LIST";
export const SELECT_CURRENCY_ITEM = "SELECT_CURRENCY_ITEM";
export const SET_CURRENCY_VALUE = "SET_CURRENCY_VALUE";
export const SET_CONVERTED_VALUE = "SET_CONVERTED_VALUE";
