import {
  SET_CURRENCY_CODE_SELECTED,
  SWAP_SELECTED_CURRENCIES,
  GET_MAIN_CURRENCIES,
  SWITCH_CARET_ICON,
  SET_CARET_ICON_SELECTED_STATUS,
  SHOW_CURRENCY_LIST,
  CLOSE_CURRENCY_LIST,
  SELECT_CURRENCY_ITEM,
  SET_CURRENCY_VALUE,
  SET_CONVERTED_VALUE,
} from "../actions/types";
import parse from "html-react-parser";
import { currenciesData } from "../Data/data";

const initialState = {
  currencies: {
    ofFirstCulcbox: [],
    ofSecondCulcbox: [],
  },

  selectedCurrencyCodes: {
    ofFirstCulcbox: ["RUB"],
    ofSecondCulcbox: ["USD"],
  },

  currencyValue: {
    ofFirstCulcbox: 1000,
    ofSecondCulcbox: "",
  },

  baseCurrency: {
    ofFirstCulcbox: "",
    ofSecondCulcbox: "",
  },

  caretIconSelectedStatus: false,
  isCurrencyListOpen: false,
  dateTime: "",
};

const containsObject = (arr, obj) => {
  return arr.some((item) => item.code === obj.code);
};

const culcboxReducer = (state = initialState, action) => {
  const replaceableCurrencyCodeIndex =
    state.currencies.ofFirstCulcbox.length - 2;

  const setActiveCodes = (culcbox) => {
    return (
      (action.payload.code && [action.payload.code]) ||
      (action.payload.index && [culcbox[0], action.payload.index])
    );
  };

  switch (action.type) {
    case SET_CURRENCY_CODE_SELECTED:
      if (action.payload.uniqueId === 0) {
        return {
          ...state,
          selectedCurrencyCodes: {
            ofFirstCulcbox: setActiveCodes(
              state.selectedCurrencyCodes.ofFirstCulcbox
            ),
            ofSecondCulcbox: [state.selectedCurrencyCodes.ofSecondCulcbox[0]],
          },
        };
      } else {
        return {
          ...state,
          selectedCurrencyCodes: {
            ofFirstCulcbox: [state.selectedCurrencyCodes.ofFirstCulcbox[0]],
            ofSecondCulcbox: setActiveCodes(
              state.selectedCurrencyCodes.ofSecondCulcbox
            ),
          },
        };
      }

    case SWAP_SELECTED_CURRENCIES:
      const isCodeAlreadyShownInFirstCulcbox =
        state.currencies.ofFirstCulcbox.some(
          (currency) => currency.code === action.payload.ofSecondCulcbox.code
        );

      const isCodeAlreadyShownInSecondCulcbox =
        state.currencies.ofSecondCulcbox.some(
          (currency) => currency.code === action.payload.ofFirstCulcbox.code
        );

      const renderWithSwappedCode = (currenciesCulcbox, payloadCulcbox) => {
        return currenciesCulcbox.map((currency, index) => {
          return index === replaceableCurrencyCodeIndex
            ? payloadCulcbox
            : currency;
        });
      };

      return {
        ...state,
        selectedCurrencyCodes: {
          ofFirstCulcbox: [action.payload.ofSecondCulcbox.code],
          ofSecondCulcbox: [action.payload.ofFirstCulcbox.code],
        },
        currencies: {
          ofFirstCulcbox: isCodeAlreadyShownInFirstCulcbox
            ? state.currencies.ofFirstCulcbox
            : renderWithSwappedCode(
                state.currencies.ofFirstCulcbox,
                action.payload.ofSecondCulcbox
              ),

          ofSecondCulcbox: isCodeAlreadyShownInSecondCulcbox
            ? state.currencies.ofSecondCulcbox
            : renderWithSwappedCode(
                state.currencies.ofSecondCulcbox,
                action.payload.ofFirstCulcbox
              ),
        },
      };

    case GET_MAIN_CURRENCIES: {
      //Получаем основные валюты, которые будут отображены на главной в UI над culcbox'ом
      const defaultCodes = ["RUB", "USD", "EUR", "GBP"];

      const filteredCurrenciesByDefault = currenciesData.filter((currency) =>
        defaultCodes.includes(currency.code)
      );

      //Пушим caret-down
      filteredCurrenciesByDefault.push(parse(`<i class="fa fa-caret-down" />`));

      const mainCurrencies = filteredCurrenciesByDefault;

      return {
        ...state,
        currencies: {
          ofFirstCulcbox: mainCurrencies,
          ofSecondCulcbox: mainCurrencies,
        },
      };
    }

    case SWITCH_CARET_ICON:
      const caretIconIndex = state.currencies.ofFirstCulcbox.length - 1;

      const switchCaretIcon = (culcbox) => {
        return culcbox.map((currency, index) => {
          if (index === caretIconIndex && action.payload.isCaretDown) {
            return parse('<i class="fas fa-caret-up" />');
          } else if (index === caretIconIndex && !action.payload.isCaretDown) {
            return parse('<i class="fa fa-caret-down" />');
          }
          return currency;
        });
      };

      const keepCaretIconDown = (culcbox) => {
        return culcbox.map((currency, index) => {
          if (index === caretIconIndex) {
            return parse('<i class="fa fa-caret-down" />');
          } else {
            return currency;
          }
        });
      };

      if (action.payload?.uniqueId === 0) {
        return {
          ...state,
          currencies: {
            ofFirstCulcbox: switchCaretIcon(state.currencies.ofFirstCulcbox),
            ofSecondCulcbox: keepCaretIconDown(
              state.currencies.ofSecondCulcbox
            ),
          },
        };
      } else if (action.payload?.uniqueId === 1) {
        return {
          ...state,
          currencies: {
            ofFirstCulcbox: keepCaretIconDown(state.currencies.ofFirstCulcbox),
            ofSecondCulcbox: switchCaretIcon(state.currencies.ofSecondCulcbox),
          },
        };
      } else {
        // если id - undefind, ситуация для обоих калкбоксов. Ставим иконку кареты закрытой, когда закрыли currencyList.
        // Вызывается из Home
        return {
          ...state,
          currencies: {
            ofFirstCulcbox: keepCaretIconDown(state.currencies.ofFirstCulcbox),
            ofSecondCulcbox: keepCaretIconDown(
              state.currencies.ofSecondCulcbox
            ),
          },
        };
      }

    case SET_CARET_ICON_SELECTED_STATUS:
      const isCaretsActiveBothCulcboxes =
        state.selectedCurrencyCodes.ofFirstCulcbox.length === 2 &&
        state.selectedCurrencyCodes.ofSecondCulcbox.length === 2;

      if (isCaretsActiveBothCulcboxes) {
        if (action.payload.uniqueId === 0) {
          return {
            ...state,
            selectedCurrencyCodes: {
              ofFirstCulcbox: state.selectedCurrencyCodes.ofFirstCulcbox,
              ofSecondCulcbox: [state.selectedCurrencyCodes.ofSecondCulcbox[0]],
            },
            caretIconSelectedStatus: action.payload.status,
          };
        } else {
          return {
            ...state,
            selectedCurrencyCodes: {
              ofFirstCulcbox: [state.selectedCurrencyCodes.ofFirstCulcbox[0]],
              ofSecondCulcbox: state.selectedCurrencyCodes.ofSecondCulcbox,
            },
            caretIconSelectedStatus: action.payload.status,
          };
        }
      }

      return {
        ...state,
        caretIconSelectedStatus: action.payload.status,
      };

    case SHOW_CURRENCY_LIST:
      return {
        ...state,
        isCurrencyListOpen: true,
      };

    case CLOSE_CURRENCY_LIST:
      return {
        ...state,
        isCurrencyListOpen: false,
      };

    case SELECT_CURRENCY_ITEM:
      let isCodeAlreadyShown = null;

      const selectCurrencyItem = (culcbox) => {
        return culcbox.map((currency, index) => {
          return index === replaceableCurrencyCodeIndex && !isCodeAlreadyShown
            ? action.payload.currency
            : currency;
        });
      };

      if (action.payload.uniqueId === 0) {
        isCodeAlreadyShown = containsObject(
          state.currencies.ofFirstCulcbox,
          action.payload.currency
        );
        return {
          ...state,
          currencies: {
            ofFirstCulcbox: selectCurrencyItem(state.currencies.ofFirstCulcbox),
            ofSecondCulcbox: state.currencies.ofSecondCulcbox,
          },
          selectedCurrencyCodes: {
            ofFirstCulcbox: [action.payload.currency.code],
            ofSecondCulcbox: state.selectedCurrencyCodes.ofSecondCulcbox,
          },
        };
      } else {
        isCodeAlreadyShown = containsObject(
          state.currencies.ofSecondCulcbox,
          action.payload.currency
        );
        return {
          ...state,
          currencies: {
            ofFirstCulcbox: state.currencies.ofFirstCulcbox,
            ofSecondCulcbox: selectCurrencyItem(
              state.currencies.ofSecondCulcbox
            ),
          },
          selectedCurrencyCodes: {
            ofFirstCulcbox: state.selectedCurrencyCodes.ofFirstCulcbox,
            ofSecondCulcbox: [action.payload.currency.code],
          },
        };
      }

    case SET_CURRENCY_VALUE:
      if (action.payload.uniqueId === 0) {
        return {
          ...state,
          currencyValue: {
            ofFirstCulcbox: action.payload.value,
            ofSecondCulcbox: action.payload.value
              ? state.currencyValue.ofSecondCulcbox
              : "",
          },
          baseCurrency: {
            ofFirstCulcbox: "",
            ofSecondCulcbox: "",
          },
        };
      } else {
        return {
          ...state,
          currencyValue: {
            ofFirstCulcbox: action.payload.value
              ? state.currencyValue.ofFirstCulcbox
              : "",
            ofSecondCulcbox: action.payload.value,
          },
          baseCurrency: {
            ofFirstCulcbox: "",
            ofSecondCulcbox: "",
          },
        };
      }

    case SET_CONVERTED_VALUE:
      const makeTwoDigitValue = (value) => {
        const twoDigitsValue = ("0" + value).slice(-2);
        return twoDigitsValue;
      };
      const convertTimestampToDate = (unixTimestamp) => {
        const date = new Date(unixTimestamp * 1000);
        return `${date.getFullYear()}-${makeTwoDigitValue(
          date.getMonth() + 1
        )}-${makeTwoDigitValue(date.getDate())} ${makeTwoDigitValue(
          date.getHours()
        )}:${makeTwoDigitValue(date.getMinutes())}:${makeTwoDigitValue(
          date.getSeconds()
        )}`;
      };

      const getExchangeRateValues = () => {
        const exchangeRateFirstCulcbox =
          state.selectedCurrencyCodes.ofSecondCulcbox[0];
        const exchangeRateSecondCulcbox =
          state.selectedCurrencyCodes.ofFirstCulcbox[0];

        const { [exchangeRateFirstCulcbox]: valueFirstCulcbox } =
          action.payload.exchangeRatesFirstCulcbox;

        const { [exchangeRateSecondCulcbox]: valueSecondCulcbox } =
          action.payload.exchangeRatesSecondCulcbox;

        return {
          firstCulcbox: valueFirstCulcbox,
          secondCulcbox: valueSecondCulcbox,
        };
      };

      if (action.payload.uniqueId === 0) {
        const exchangeRateValueFirstCulcbox =
          getExchangeRateValues().firstCulcbox;
        const currencyValue = action.payload.value
          ? action.payload.value
          : state.currencyValue.ofFirstCulcbox;
        const convertedValue = currencyValue * exchangeRateValueFirstCulcbox;

        return {
          ...state,
          currencyValue: {
            ofFirstCulcbox: state.currencyValue.ofFirstCulcbox,
            ofSecondCulcbox:
              (+convertedValue > 0.0001
                ? +convertedValue.toFixed(4)
                : +convertedValue) || currencyValue,
          },
          baseCurrency: {
            ofFirstCulcbox: exchangeRateValueFirstCulcbox,
            ofSecondCulcbox: getExchangeRateValues().secondCulcbox,
          },
          dateTime: convertTimestampToDate(action.payload.lastUpdated),
        };
      } else {
        const exchangeRateValueSecondCulcbox =
          getExchangeRateValues().secondCulcbox;
        const currencyValue = action.payload.value
          ? action.payload.value
          : state.currencyValue.ofSecondCulcbox;
        const convertedValue = currencyValue * exchangeRateValueSecondCulcbox;

        return {
          ...state,
          currencyValue: {
            ofFirstCulcbox:
              (+convertedValue > 0.0001
                ? +convertedValue.toFixed(4)
                : +convertedValue) || currencyValue,
            ofSecondCulcbox: state.currencyValue.ofSecondCulcbox,
          },
          baseCurrency: {
            ofFirstCulcbox: getExchangeRateValues().firstCulcbox,
            ofSecondCulcbox: exchangeRateValueSecondCulcbox,
          },
          dateTime: convertTimestampToDate(action.payload.lastUpdated),
        };
      }

    default:
      return state;
  }
};

export default culcboxReducer;
