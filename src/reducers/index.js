import { combineReducers } from "redux";
import culcboxReducer from "./culcboxReducer";

const rootReducer = combineReducers({
  culcbox: culcboxReducer,
});

export default rootReducer;
